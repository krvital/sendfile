<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name;
?>

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'file-form',
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data'
        ),
    ))
    ?>
    <?php echo $form->errorSummary($file); ?>
    <input type="hidden" name="<?php echo ini_get("session.upload_progress.name") ?>" value="file"/>
    <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>

    <div class="row">
        <?php echo $form->fileField($file, 'name'); ?>
        <?php echo $form->error($file, 'name'); ?>
    </div>

    <div class="button">
        <?php echo CHtml::submitButton('Загрузить файл', array(
            'id' => 'sButton'
        )); ?>
    </div>
    <?php $this->endWidget() ?>


</div>
