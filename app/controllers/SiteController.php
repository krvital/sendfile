<?php

class SiteController extends Controller
{

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {
        $model = new File();

        $this->performAjaxValidation($model);

        if (isset($_POST['File'])) {
            $model->name = CUploadedFile::getInstance($model, 'name');
            if($model->validate()) {
                $model->hash = md5_file($model->name->tempName);
            }

            $oldModel = File::model()->findByAttributes(array('hash' => $model->hash));

            if ($oldModel === null) {
                if ($model->save()) {
                    $model->name->saveAs($model->getFilesPath() . '/' . $model->name->name);
                    $this->redirect(array('view', 'hash' => $model->hash));
                }
            } else {
                $this->redirect(array('view', 'hash' => $oldModel->hash));
            }
        }

        $this->render('index', array(
            'file' => $model
        ));
    }


    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionView($hash)
    {
        $model = File::model()->findByAttributes(array('hash' => $hash));
        if($model === null ) {
            throw new CHttpException(404, 'There is no file here');
        }
        $this->render('view', array('model' => $model));
    }

    public function actionDownload($hash)
    {
        $file = File::model()->findByAttributes(array('hash' => $hash));
        if ($file === null) {
            throw new CHttpException(404, 'There is no file here');
        }

        if (file_exists($file->getPath())) {
            if (ob_get_level()) {
                ob_end_clean();
            }
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file->getPath()));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file->getPath()));
            readfile($file->getPath());
            exit;
        }
    }

    public function actionDelete($hash)
    {
        $file = File::model()->findByAttributes(array('hash' => $hash));
        if ($file === null) {
            throw new CHttpException(404, 'There is no file here');
        }
        $this->render('delete', array('id' => $file->id));
    }

    public function actionConfirmDelete($id)
    {
        $model = File::model()->findByPk($id);
        if ($model !== null) {
            $model->delete();
        }
        $this->redirect('index');
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'file-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}