<?php

class m131001_042157_init_migration extends CDbMigration
{
	public function safeUp()
	{
        $this->createTable('file', array(
            'id' => 'pk',
            'hash' => 'varchar(40) NOT NULL',
            'name' => 'string NOT NULL'
        ));
	}


	public function safeDown()
	{
        $this->dropTable('file');
	}
}